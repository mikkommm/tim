from timApp.timdb.sqa import db


class UserGamification(db.Model):
    """Created by TIMG This class represents the UserGamification database table, used connecting users to gamification
    documents."""
    __tablename__ = 'usergamification'
    gamification_doc_id = db.Column(db.Integer, db.ForeignKey('gamificationdocument.id'), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('useraccount.id'), primary_key=True)
    is_gamified = db.Column(db.Boolean)

    @staticmethod
    def create(game_doc_id: int, user_id: int, is_gamified: bool) -> 'UserGamification':
        """Creates a new entry into DocGamified table."""

        user_gamification = UserGamification(game_doc_id, user_id, is_gamified)
        db.session.add(user_gamification)
        return user_gamification
