from languages import *
from jsframe import *
from stack import *
from geogebra import *

languages["jypeli"] = Jypeli
languages["comtest"] = CSComtest
languages["shell"] = Shell
languages["cs"] = CS
languages["java"] = Java
languages["graphics"] = Graphics
languages["jcomtest"] = JComtest
languages["junit"] = JUnit
languages["scala"] = Scala
languages["cc"] = CC
languages["c++"] = CPP
languages["ccomtest"] = CComtest
languages["py"] = PY3
languages["py2"] = PY2
languages["swift"] = Swift
languages["lua"] = Lua
languages["clisp"] = CLisp
languages["text"] = Text
languages["xml"] = XML
languages["css"] = Css
languages["jjs"] = JJS
languages["js"] = JS
languages["glowscript"] = Glowscript
languages["vpython"] = VPython
languages["sql"] = SQL
languages["psql"] = PSQL
languages["alloy"] = Alloy
languages["run"] = Run
languages["md"] = MD
languages["html"] = HTML
languages["simcir"] = SimCir
languages["sage"] = Sage
languages["r"] = R
languages["fs"] = FS
languages["mathcheck"] = Mathcheck
languages["upload"] = Upload
languages["octave"] = Octave
languages["processing"] = Processing
languages["wescheme"] = WeScheme
languages["ping"] = Ping
languages["kotlin"] = Kotlin
languages["fortran"] = Fortran
languages["rust"] = Rust
languages["pascal"] = Pascal
languages["stack"] = Stack
languages["geogebra"] = Geogebra
languages["jsav"] = Jsav
languages["quorum"] = Quorum
languages["jsframe"] = JSframe
languages["chartjs"] = ChartJS
