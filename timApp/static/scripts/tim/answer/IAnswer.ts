export interface IAnswer {
    content: string;
    id: number;
    points?: number;
    last_points_modifier: number;
    valid: boolean;
}
