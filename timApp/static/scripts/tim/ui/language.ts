export const FRONT_PAGE_DEFAULT_LANGUAGE = "fi";
// Currently there's no language switch outside start page, so if user doesn't
// visit the start page with its language selection, login (like the rest of UI) will be in English.
export const LOGIN_DEFAULT_LANGUAGE = "en";
